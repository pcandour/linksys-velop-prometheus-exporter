#!/usr/bin/python3

import requests
import http.server
import threading
import socket
import subprocess
import time


def getPing():
    output = subprocess.check_output(["/bin/ping", "-c", "1", "-qn", "1.1.1.1"], universal_newlines=True)
    return output.split('\n')[4].split('/')[4]


def parseIfconfig(lines):
    adaptor = lines[0].split(' ')[0]
    for line in lines[4:]:
        if line.startswith('RX bytes'):
            bytecounts = line.lstrip().split(' ')
            rx = bytecounts[1][6:]
            tx = bytecounts[6][6:]

    return (adaptor, rx, tx)


def getAdaptorStats():
    ifconfig = []
    reading = False
    readingtop = False

    start = time.time()
    r = requests.get('http://admin:ADMINPASSWORDGOESHERE@192.168.1.1/sysinfo.cgi')
    end = time.time()
    metrics = PromGauge('sysinfo_s', '', end - start)

    for line in r.text.split('\n'):
        line = line.strip()
        if line.startswith('ifconfig:'):
            reading = True
        elif line.startswith('top -bn1'):
            readingtop = True
        elif reading:
            if line == '':
                if ifconfig:
                    adaptor = parseIfconfig(ifconfig)
                    metrics = (metrics + PromCounter('recv_bytes', 'if="%s"' % adaptor[0], adaptor[1])
                        + PromCounter('send_bytes', 'if="%s"' % adaptor[0], adaptor[2]))
                    ifconfig = []
            elif line == 'cat /etc/resolv.conf:':
                reading = False
            else:
                ifconfig.append(line)
        elif readingtop:
            if line.startswith('Mem'):
                p = list(filter(None, line.split(' ')))
                memused = p[1][:-1]
                memfree = p[3][:-1]
                metrics = metrics + PromGauge('memtotal', 'host="router"', int(memused)+int(memfree)) + PromGauge('memfree', 'host="router"', memfree)
            elif line.startswith('CPU'):
                p = list(filter(None, line.split(' ')))
                metrics = metrics + PromGauge('cpu', 'host="router",ring="user"', p[1][:-1]) + PromGauge('cpu', 'host="router",ring="sys"', p[3][:-1])
            

    return metrics


def getPiMemoryStats():
    results = {'memtotal': 0, 'memfree': 0}
    with open('/proc/meminfo', 'r') as f:
        for line in f:
            if line.startswith('MemTotal'):
                p = list(filter(None, line.split(' ')))
                results['memtotal'] = p[1]
            if line.startswith('MemFree'):
                p = list(filter(None, line.split(' ')))
                results['memfree'] = p[1]
    return results


def PromHelp(name, ctype):
    return "# HELP %s\n# TYPE %s %s\n" % (name, name, ctype)


def PromCounter(name, labels, value):
    return "%s{%s} %s\n" % (name, labels, value)


def PromGauge(name, labels, value):
    return "%s{%s} %s\n" % (name, labels, value)


class MyHttpRequestHandler(http.server.SimpleHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "text/plain")
        self.end_headers()

        self.wfile.write(bytes(metrics, "utf8"))

        return


print('Getting initial metrics...')
ram = getPiMemoryStats()
metrics = (
    PromHelp('memtotal', 'gauge') +
    PromGauge('memtotal', 'host="pi"', ram['memtotal']) +
    PromHelp('memfree', 'gauge') +
    PromGauge('memfree', 'host="pi"', ram['memfree']) +
    PromHelp('recv_bytes', 'counter') + 
    PromHelp('send_bytes', 'counter') + 
    getAdaptorStats() + 
    PromHelp('PingRtt', 'gauge') +
    PromGauge('PingRtt', 'host="pi"', getPing()))

print('Serving')

server = http.server.HTTPServer(("", 8080), MyHttpRequestHandler)
thread = threading.Thread(target = server.serve_forever)
thread.daemon = True
thread.start()

while True:
    time.sleep(60)
    print('Refreshing')
    ram = getPiMemoryStats()
    metrics = (
        PromHelp('memtotal', 'gauge') +
        PromGauge('memtotal', 'host="pi"', ram['memtotal']) +
        PromHelp('memfree', 'gauge') +
        PromGauge('memfree', 'host="pi"', ram['memfree']) +
        PromHelp('recv_bytes', 'counter') + 
        PromHelp('send_bytes', 'counter') + 
        getAdaptorStats() + 
        PromHelp('PingRtt', 'gauge') +
        PromGauge('PingRtt', 'host="pi"', getPing()))
    print('Refreshed')

