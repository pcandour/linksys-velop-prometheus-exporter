# README #

Very quick and dirty script to scrape metrics from a Linksys Velop router and export them for Prometheus.

To use:

Edit exporter.py and make sure the IP address and password are correct for your router. Then:

`python3 exporter.py`

Or to background it (you probably want this):

`nohup python3 exporter.py &`


